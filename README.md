# Spread of the threshold vs. `LDAC_LIN` measurement for RD53A

This is a small tool for measuring pixel threshold spread as a function of `LDAC_LIN` using the `Ph2_ACF` framework **on RD53A chip**.
The tool consists of a shell script (`LDAC_LIN_measurement.sh`) and a ROOT macro (`LDAC_LIN_calibration.C`). The shell script performs all the needed measurements (*Threshold Equalization* and *S-curve* measurements for each point of `LDAC_LIN`) and needs to be ran first. The ROOT macro analyzes the measured data and finds the best `LDAC_LIN` value so it needs to be ran when all the measurements are finished.

There is also a script for RD53B available in [another branch](https://gitlab.cern.ch/mambroza/thrspread_vs_ldac_lin/-/tree/RD53B).

Four versions of the measurement are available:
1. The "full" version: *Threshold Equalization* is followed by an *S-curve* measurement for each of 23 (or as many as you like) points with different `LDAC_LIN` values. The best value of `LDAC_LIN` is determined by the smallest spread of the threshold (which we get from S-curves).
2. The "short" version: only *Threshold Equalization* is ran for each value of `LDAC_LIN` (no *S-curve*). The "best" value is chosen by the smallest spread of pixel efficiencies at nominal threshold. This method only aproximately shows where the real best `LDAC_LIN` may be (from the tests made so far it seems that real best value is a bit higher than the one given by the "short" version).
3. The "normal" version: only *Threshold Equalization* is ran for each value of `LDAC_LIN` (no *S-curve*). 7 Best points are selected from the spread of pixel efficiencies at nominal threshold. *S-curve* measurements are lated done on these points only.
4. The "extra" version: does only the second part of the "normal" version. Do this if you have done the "short" version previously.

You might want to check [this short presentation](https://indico.cern.ch/event/1069283/contributions/4496542/attachments/2300018/3912301/IT_DAQ_MarijusAmbrozas_21-08-30.pdf) to see the idea and what to expect.

### Instructions on how to use the tool:
1. Make sure you have the `Ph2_ACF` software ready and compiled on your setup. If you don't, please first make sure you clone the `Ph2_ACF` repository and compile the software by following the instructions provided [here](https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF).
2. Make sure your hardware is runing properly (e.g., by running *Pixelalive*) and that you have ran the *Threshold Adjustment* beforehand and have the nominal threshold set to the value of your liking.
3. Clone this repository (there is not much difference where exactly you do it but it is recommended you do it inside `Ph2_ACF` directory or inside your working directory in `Ph2_ACF`):
```
git clone ssh://git@gitlab.cern.ch:7999/mambroza/thrspread_vs_ldac_lin.git
```
4. Copy the xml and register files that you use to the `thrspread_vs_ldac_lin` directory:
```
# assuming you use the default settings and have this repository cloned inside Ph2_ACF directory
cp settings/CMSIT.xml thrspread_vs_ldac_lin/
cp settings/CMSIT_RD53.txt thrspread_vs_ldac_lin/
```
5. Make sure your xml file is called `CMSIT.xml`, otherwise you will have to specify it with a flag "-f".
6. Run the shell script (it will run Threshold Equalization and S-curve 23 (or as many as you like) times with different `LDAC_LIN` values):
```
./LDAC_LIN_measurement.sh
# Use flag "-s" to run the "short" version of "-l" for the "long" version
# ./LDAC_LIN_measurement.sh -s
```
7. There are many different flags available:<br>
   7.1. `-h` or `--help` – show brief help<br>
   7.2. `-s` or `--short` – do the "short" measurement version<br>
   7.3. `-l` or `--long` – do the "long" measurement version<br>
   7.4. `-e` or `--extra` – do the "extra" measurement version<br>
   7.5. `-o` or `--overwrite` – overwrite measurement root files if they already exist (default is to just skip the measurement if the file already exists)<br>
   7.6. `-a` or `--analyze` – automatically run the root analysis macro after the measurement is done (without the graphical outputs)<br>
   7.7. `-f XML.xml` or `--file=XML.xml` – specify the xml file (default is `CMSIT.xml`)<br>
   7.8. `-m measurementSet` or `--measurement=measurementSet` – specify the measurement set (in short, it is the output directory name, useful if you are doing many measurements to keep track of them)<br>
   7.9. `-v values.txt` or `--values=values.txt` – specify the text file containing all the LDAC_LIN values you want to try. If not specified, a default set of 23 points will be used.
   
8. Run the ROOT macro to analyze the results (you should specify the measurement set as a string argument):
```
root -l
.x LDAC_LIN_calibration.C("myChip_20")
// If you ran the "short" measurement, you need to specify the second string argument as "SHORT" (or "S") to the ROOT macro
// Analogically, specify the argument "LONG" (or "L") for the "long" measurement
// Leave an empty string ("") for the "normal" measurement
// .x LDAC_LIN_calibration.C("myChip_20", "SHORT")
```
9. You should see a graph showing the spread of the threshold vs. `LDAC_LIN` (spread of efficiencies vs. `LDAC_LIN` if running the "short" version). A clear minimum should be apparent.
10. You may repeat the measurement at different conditions (e.g., different temperatures) to check if the result is stable.

If you have any questions and/or suggestions, please contact Marijus Ambrozas (marijus.ambrozas@cern.ch).
